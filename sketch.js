// By Ricardo Arreola,
// Create triangle patterns;
// click to slow down and 's' to save as image.
var xo, yo, plus;
var times = 30;
var scal = 1;
var colorRange = 800;
var colorCounter = 0;
var fRate = 30;
var pause = false;

function setup() {
  if (windowWidth>=windowHeight){
  this.winSize =  windowHeight;
      }
  else{
   this.winSize =  windowWidth;
  }
  createCanvas(this.winSize, this.winSize);
  background(240);
  frameRate(fRate);
  noFill();
  xo = width/2;
  yo = height/2;
  plus = TWO_PI/times;
  
}



function draw() {
  colorMode(HSB, colorRange);
  stroke(colorCounter, colorRange, colorRange)
  colorCounter = colorCounter + 1;
  tri(frameCount*scal, plus*frameCount)
}

function tri(siz, ang){
  plu = times/3;
  let x1 = xo + sin(ang)*siz;
  let y1 = yo + cos(ang)*siz;
  
  let x2 = xo + sin(ang+plu*plus)*siz;
  let y2 = yo + cos(ang+plu*plus)*siz;
  
  let x3 = xo + sin(ang-plu*plus)*siz;
  let y3 = yo + cos(ang-plu*plus)*siz;
  
  triangle(x1,y1,x2,y2,x3,y3);
  
}

function mousePressed(){
  pause = !pause;
  if (pause){
    frameRate(1);
  }
  else{
    frameRate(fRate);
  }
}
function keyPressed() {
  if (keyCode === 83) {
    save("HypnoTriangles.png")
  }
}